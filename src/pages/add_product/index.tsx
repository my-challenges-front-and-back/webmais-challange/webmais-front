/* react */
import { useState, useRef } from 'react'
import Header from '../../components/Header'
import Main from '../../components/Main/Main'
import InputText from '../../components/InputText'
import Button from '../../components/Button'

/* unform */
import { Form } from '@unform/web'
import { FormHandles } from '@unform/core'

/* yup */
import * as Yup from 'yup'

/* icons */
import { BsFillInfoCircleFill } from 'react-icons/bs'
import { AiOutlineMinus, AiOutlinePlus } from 'react-icons/ai'
import { FiCopy } from 'react-icons/fi'
import Input from '../../components/Input'
import Alert from '../../components/Alert'
import axios from 'axios'
import getValidationErrors from '../../utils/error_handle'

interface CommentProperty {
  comment: string
}

interface Product {
  title: string
  description: string
  code: string
  width: number
  height: number
  length: number
}

const AddProduct = () => {
  const [comments, setComments] = useState<Array<CommentProperty>>([])
  const [inputCommetQnt, setCommentInputQnt] = useState(1)

  const [show, setShowAlert] = useState(false)
  const [messageAlert, setMessageAlert] = useState('')
  const [typeError, setTypeError] = useState('error')

  /* inputs control */
  const [titleOfProduct, setTitleOfProduct] = useState('')
  const [codeOfProduct, setCodeProduct] = useState('')
  const [descriptionOfProduct, setDescriptionOfProduct] = useState('')
  const [widthOfProduct, setWidthOfProduct] = useState('')
  const [heightOfProduct, setHeightOfProduct] = useState('')
  const [lengthOfProduct, setLengthOfProduct] = useState('')
  const [priceOfProduct, setPriceOfProduct] = useState('')

  const formRef = useRef<FormHandles>(null)

  const onChangeComment = (
    event: React.ChangeEvent<HTMLTextAreaElement>,
    indice: number
  ) => {
    const copyComments = comments
    if (copyComments[indice]) {
      copyComments[indice].comment = event.currentTarget.value
    }
    if (!copyComments[indice]) {
      copyComments.push({
        comment: event.currentTarget.value
      })
    }

    setComments([...copyComments])
  }

  const removeCommentInput = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    index: number
  ) => {
    event.preventDefault()
    setCommentInputQnt(inputCommetQnt - 1)
    const copyComments = comments
    copyComments.splice(index, 1)
    setComments(copyComments)
  }

  const addInputComment = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    event.preventDefault()
    setCommentInputQnt(inputCommetQnt + 1)
  }

  const renderInputsComments = Array.from(
    { length: inputCommetQnt },
    (_, index) => (
      <div className="w-full mt-3" key={index}>
        <div className="flex flex-col flex-1 items-center ">
          <div className="mb-3 w-full relative">
            <InputText
              placeholder={`Seu comentário ${index > 0 ? index : ''}`}
              onChange={(e) => onChangeComment(e, index)}
              value={comments[index]?.comment}
            />
            {index > 0 && (
              <Button
                className="w-8 h-8 bg-red-300 rounded-full absolute flex justify-center items-center right-0"
                style={{ top: '-15px' }}
                onClick={(e) => removeCommentInput(e, index)}
              >
                <AiOutlineMinus className="text-red-500" />
              </Button>
            )}
          </div>
        </div>
      </div>
    )
  )

  const clearedInputs = () => {
    setTitleOfProduct('')
    setCodeProduct('')
    setDescriptionOfProduct('')
    setWidthOfProduct('')
    setHeightOfProduct('')
    setLengthOfProduct('')
    setPriceOfProduct('')
    setComments([{ comment: '' }])
  }

  const formatNumber = (num: string) => {
    const transform = num.replace(/\./g, '').replace(',', '.')

    const numModifier = parseFloat(transform)

    return numModifier
  }

  const handleForm = async (data: Product) => {
    try {
      formRef.current?.setErrors({})
      const schemaProduct = Yup.object({
        title: Yup.string().required('Por favor, Informe o título'),
        code: Yup.string(),
        width: Yup.string().required('Por favor, Informe a largura'),
        height: Yup.string().required('Por favor, Informe a altura'),
        length: Yup.string().required('Por favor, informe a largura')
      })

      await schemaProduct.validate(data, { abortEarly: false })

      await axios.post('http://localhost:3001/product', {
        title: titleOfProduct,
        description: descriptionOfProduct,
        code: codeOfProduct,
        width: formatNumber(widthOfProduct),
        height: formatNumber(heightOfProduct),
        length: formatNumber(lengthOfProduct),
        comments
      })
      setMessageAlert('Produto Registrado!')
      setShowAlert(true)
      setTypeError('success')
      clearedInputs()
      setCommentInputQnt(1)
    } catch (erro: any) {
      if (erro instanceof Yup.ValidationError) {
        const errors = getValidationErrors(erro)
        formRef.current?.setErrors(errors)
        setMessageAlert(erro?.errors[0])
        setShowAlert(true)
        setTypeError('error')
        return
      }
      setMessageAlert(erro?.response?.data?.error)
      setShowAlert(true)
      setTypeError('error')
    }
  }

  return (
    <>
      <Header />
      <Main>
        <Alert
          message={messageAlert}
          show={show}
          type={typeError}
          setShow={setShowAlert}
        />
        <div className="md:w-1/2 sm>w-full">
          <Form onSubmit={handleForm} ref={formRef}>
            <section className="flex flex-col  border-b-2 border-dashed pb-3 mb-6">
              <div className="flex items-center justify-between mb-4">
                <div className="flex items-center">
                  <BsFillInfoCircleFill color="#cdcdcd" />
                  <h2 className="ml-4">Informações gerais</h2>
                </div>
                <div className="flex items-center">
                  <h2>
                    Campos obrigatorios (<span className="text-red-500">*</span>
                    )
                  </h2>
                </div>
              </div>
              <div className="mt-8 flex">
                <div className="flex flex-col flex-1">
                  <h6 className="text-base">
                    Nome do Produto <span className="text-red-500">*</span>
                  </h6>
                  <span className="text-font-default text-xs mt-1 w-11/12">
                    Caso o código não seja informado será gerado
                    automaticamente.
                  </span>
                </div>
                <div className="flex flex-col flex-1 items-center ">
                  <div className="mb-3 w-full">
                    <Input
                      placeholder="EX: Iphone XR 16GB"
                      required
                      name="title"
                      value={titleOfProduct}
                      onChange={(e) => setTitleOfProduct(e.currentTarget.value)}
                    />
                  </div>
                  <div className="mb-3 w-full">
                    <Input
                      IconElement={FiCopy}
                      name="code"
                      placeholder="EX: F2A1ada"
                      value={codeOfProduct}
                      onChange={(e) => setCodeProduct(e.currentTarget.value)}
                    />
                  </div>
                </div>
              </div>
            </section>
            <section className="flex flex-col  border-b-2 border-dashed pb-3">
              <div className=" flex">
                <div className="flex flex-col flex-1">
                  <h6 className="text-base">Descrição do Produto</h6>
                </div>
                <div className="flex flex-col flex-1 items-center ">
                  <div className="mb-3 w-full">
                    <InputText
                      placeholder="Digite sua descrição"
                      value={descriptionOfProduct}
                      onChange={(e) =>
                        setDescriptionOfProduct(e.currentTarget.value)
                      }
                    />
                  </div>
                </div>
              </div>
            </section>
            <section className="flex flex-col  border-b-2 border-dashed pb-3 mb-6 ">
              <div className="mt-8 flex">
                <div className="flex flex-col flex-1">
                  <h6 className="text-base">
                    Largura <span className="text-red-500">*</span>
                  </h6>
                </div>
                <div className="flex flex-col flex-1 items-center ">
                  <div className="mb-3 w-full">
                    <Input
                      placeholder="0,00"
                      name="width"
                      pattern="[0-9]*[.,]?[0-9]+"
                      inputMode="numeric"
                      required
                      value={widthOfProduct}
                      onChange={(e) => setWidthOfProduct(e.currentTarget.value)}
                    />
                  </div>
                </div>
              </div>
              <div className="flex">
                <div className="flex flex-col flex-1">
                  <h6 className="text-base">
                    Altura <span className="text-red-500">*</span>
                  </h6>
                </div>
                <div className="flex flex-col flex-1 items-center ">
                  <div className="mb-3 w-full">
                    <Input
                      placeholder="0,00"
                      pattern="[0-9]*[.,]?[0-9]+"
                      inputMode="numeric"
                      name="height"
                      required
                      value={heightOfProduct}
                      onChange={(e) =>
                        setHeightOfProduct(e.currentTarget.value)
                      }
                    />
                  </div>
                </div>
              </div>
              <div className="flex">
                <div className="flex flex-col flex-1">
                  <h6 className="text-base">
                    Comprimento <span className="text-red-500">*</span>
                  </h6>
                </div>
                <div className="flex flex-col flex-1 items-center ">
                  <div className="mb-3 w-full">
                    <Input
                      placeholder="0,00"
                      pattern="[0-9]*[.,]?[0-9]+"
                      inputMode="numeric"
                      name="length"
                      required
                      value={lengthOfProduct}
                      onChange={(e) =>
                        setLengthOfProduct(e.currentTarget.value)
                      }
                    />
                  </div>
                </div>
              </div>
            </section>
            <section className="flex flex-col  border-b-2 border-dashed pb-3 mb-6">
              <div className="flex">
                <div className="flex flex-col flex-1">
                  <h6 className="text-base">Preço</h6>
                </div>
                <div className="flex flex-col flex-1 items-center ">
                  <div className="mb-3 w-full">
                    <Input
                      placeholder="R$ 0,0"
                      pattern="[0-9]*[.,]?[0-9]+"
                      name="price"
                      inputMode="numeric"
                      value={priceOfProduct}
                      onChange={(e) => setPriceOfProduct(e.currentTarget.value)}
                    />
                  </div>
                </div>
              </div>
            </section>
            <section className="flex flex-col   border-dashed pb-3 mb-6">
              <div className="w-full">
                <div className="flex flex-1 justify-between ">
                  <h6 className="text-base">Comentarios</h6>
                  <Button
                    className="text-xs flex items-center bg-green-300 p-3 text-white rounded-md"
                    onClick={addInputComment}
                  >
                    <AiOutlinePlus />
                    <span className="ml-2 font-medium">Novo Campo</span>
                  </Button>
                </div>
                {renderInputsComments}
              </div>
            </section>
            <div className="flex"></div>
            <Button className="bg-button-color px-5 py-2 w-full rounded text-white font-medium hover:bg-button-hover delay-75 transition-colors">
              Cadastrar
            </Button>
          </Form>
        </div>
      </Main>
    </>
  )
}

export default AddProduct
