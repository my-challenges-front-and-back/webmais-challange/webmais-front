import { InputHTMLAttributes } from 'react'

interface InputProps extends InputHTMLAttributes<HTMLTextAreaElement> {
  IconElement?: React.FC<{ color: string; cursor: string }>
}

const InputText: React.FC<InputProps> = ({ ...children }) => {
  return (
    <textarea
      className="w-full bg-back-default px-2 py-5 rounded outline-gray-200 text-xs resize-none h-36"
      {...children}
    ></textarea>
  )
}

export default InputText
