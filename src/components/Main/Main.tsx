interface MainProps {
  children: React.ReactNode
}

const Main: React.FC<MainProps> = ({ children }) => {
  return (
    <main className="px-10 py-14  font-medium flex items-center flex-col">
      {children}
    </main>
  )
}

export default Main
