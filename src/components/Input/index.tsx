import { useEffect } from 'react'
import { InputHTMLAttributes, useRef } from 'react'

/* unform */
import { useField } from '@unform/core'

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  IconElement?: React.FC<{ color: string; cursor: string }>
  name: string
}

const Input: React.FC<InputProps> = ({ IconElement, name, ...rest }) => {
  const inputRef = useRef<HTMLInputElement>(null)

  const copyContentOfInput = async () => {
    const valueText = inputRef?.current?.value
    navigator.clipboard.writeText(valueText as string)
  }

  const { fieldName, registerField, error } = useField(name)

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value'
    })
  }, [fieldName, registerField])

  return (
    <div className="input-area flex bg-back-default w-full rounded relative">
      <input
        type="text"
        {...rest}
        className={`flex-1 bg-back-default rounded p-3 outline-gray-200  text-xs border-2 ${
          error ? 'border-red-300' : 'border-0'
        }`}
        ref={inputRef}
      />
      {IconElement && (
        <div
          className="icon w-10 flex justify-center items-center absolute right-0 top-3"
          onClick={copyContentOfInput}
        >
          <IconElement color="#cdcdcd" cursor="pointer" />
        </div>
      )}
    </div>
  )
}

export default Input
