import { useEffect } from 'react'

interface AlertProps {
  message: string
  type: string
  show: boolean
  setShow: React.Dispatch<React.SetStateAction<boolean>>
}

let timer: ReturnType<typeof setTimeout>

const Alert: React.FC<AlertProps> = ({ message, show, type, setShow }) => {
  const closeAlert = () => {
    setShow(false)
  }

  useEffect(() => {
    if (timer) clearTimeout(timer)
    timer = setTimeout(closeAlert, 3000)
  }, [show])

  return (
    <>
      {show && type == 'success' && (
        <div
          className={`fixed z-20 px-4 py-3 leading-normal text-blue-700 bg-blue-100 rounded-lg fixed transition-colors  duration-200 ${
            show ? 'block' : 'hidden'
          }`}
          role="alert"
          style={{ top: '30px' }}
        >
          <p className="text-base">{message}</p>
        </div>
      )}
      {show && type == 'error' && (
        <div
          className={`fixed z-20 py-3 pl-4 pr-10 leading-normal text-red-700 bg-red-100 rounded-lg transition-colors duration-200 ${
            show ? 'block' : 'hidden'
          }`}
          role="alert"
          style={{ top: '30px' }}
        >
          <p className="text-base">{message}</p>
          <span className="absolute inset-y-0 right-0 flex items-center mr-4">
            <svg
              className="w-4 h-4 fill-current"
              role="button"
              viewBox="0 0 20 20"
            >
              <path
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clip-rule="evenodd"
                fill-rule="evenodd"
              ></path>
            </svg>
          </span>
        </div>
      )}
    </>
  )
}

export default Alert
