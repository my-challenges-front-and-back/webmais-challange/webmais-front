const Header = () => {
  return (
    <header className="w-full py-5  border-2 border-solid border-gray-100 flex">
      <div className="flex w-max px-10 border-r-2 border-solid border-gray-100">
        <img src="/assets/img/svg-edited.svg" width={120} />
      </div>
      <div className="pl-7 flex">
        <h4 className="font-medium">Adicionando Novo Produto</h4>
      </div>
    </header>
  )
}

export default Header
