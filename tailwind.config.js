/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./src/**/*.{html,js,ts,tsx}"
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Poppins'],
      },
      colors: {
        'font-default': '#e5e5e5',
        'back-default': '#f9f9f9',
        'button-color': '#4E95F2',
        'button-hover': '#78AFF7'
      },
      borderColor: {
        'b-color': '#ebedee'
      }
    },
  },
  plugins: [],
}

