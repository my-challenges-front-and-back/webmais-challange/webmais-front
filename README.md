# <p align = "center"> WebMais Sistemas - Front-End </p>

<p align="center">
   <img src="https://files.fm/thumb_show.php?i=mq6vzmsr4" width="100%">
</p>


## Objective

challenge proposed by the webmais system to improve the product registration where the user can enter the code manually and the width, height and length of the product

## :computer: Technologies

- TypeScript
- ReactJS
- Eslint
- Prettier
- ReactIcons
- Axios
- Responsiveness

<br />

## How to run application ?


## 1 - First Step

```
  - Clone the repository: https://gitlab.com/my-challenges-front-and-back/webmais-challange/webmais-front.git
```



## 2 - Second Step

```
- Navigate into the cloned repository
```


## 3 - Third step

```
  yarn install or npm install
```

## 4 - Fourth step 

```
  yarn dev or npm run dev
```

```
  app running in port http://localhost:5174/
```